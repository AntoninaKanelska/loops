/*
1. Цикл — це  повторення  блоку коду кілька разів, стільки вказано в умові.
2. Існують три основні види циклів:
- for.
- while.
- do-while.
3. При while умова перевіряється на самому початку, і якщо вона хибна то тіло циклу може не виконатись взагалі.
А при do-while умова перевіряється після виконання  циклу. Тобто цикл виконається хоча б один раз.


 */
// Функція для перевірки, чи є введене значення числом
function isNumber(value) {
    return !isNaN(value) && value.trim() !== '';
}

let num1;
while (true) {
    num1 = prompt("Введи перше число:");
    if (isNumber(num1)) {
        num1 = Number(num1);
        break;
    } else {
        alert("Ти ввів не число. Спробуйте ще раз.");
    }
}

let num2;
while (true) {
    num2 = prompt("Введи друге число:");
    if (isNumber(num2)) {
        num2 = Number(num2);
        break;
    } else {
        alert("Ти ввів не число. Спробуйте ще раз.");
    }
}

let min = Math.min(num1, num2);
let max = Math.max(num1, num2);

for (let i = min; i <= max; i++) {
    console.log(i);
}



function isNumber(value) {
    return !isNaN(value) && value.trim() !== '';
}
function isEven(number) {
    return number % 2 === 0;
}
let num;
while (true) {
    num = prompt("Введи парне число:");
    if (isNumber(num)) {
        num = Number(num);
        if (isEven(num)) {
            alert(`Ти ввів парне число: ${num}`);
            break;
        } else {
            alert("Ти ввів непарне число. Спробуй ще раз.");
        }
    } else {
        alert("Ти ввів не число. Спробуй ще раз.");
    }
}